echo off
del a:*.zip
zip a:jssrc *.src *.pds *.bat bt?.* -es
zip a:jsdat data\*.* chars\*.* music\*.* orig\*.* sprts\*.* -r -P -es
chkdsk
echo Backup complete.
